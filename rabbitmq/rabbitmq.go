package rabbitmq

import (
	"fmt"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/streadway/amqp"
	schema "gitlab.com/neveragny/airutil/lib"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

// func getRabbitMqChan() *amqp.Channel {
// 	ch, conn, err := dialRabbitmq(redisConfig)
// 	if err != nil {
// 		failOnError(err, "Failed to get conn, channel from DialRabbitmq()")
// 	}
// 	defer func() {
// 		ch.Close()
// 		conn.Close()
// 	}()

// 	// tells RabbitMQ not to give more than one message to a worker at a time
// 	// https://www.rabbitmq.com/tutorials/tutorial-two-go.html
// 	err = ch.Qos(1, 0, false)
// 	if err != nil {
// 		panic(err)
// 	}
// }

// DialRabbitmq return shared connection link
func DialRabbitmq(conf *schema.RabbitMQConf) (*amqp.Channel, *amqp.Connection, error) {
	return dialRabbitmq(conf)
}

func dialRabbitmq(conf *schema.RabbitMQConf) (*amqp.Channel, *amqp.Connection, error) {
	rabbitConnectionString := fmt.Sprintf("amqp://%s:%s@%s", conf.Username, conf.Password, conf.Hosts[0])
	// log.Println(rabbitConnectionString)
	var conn *amqp.Connection
	var ch *amqp.Channel
	var err error
	for {
		conn, err = amqp.Dial(rabbitConnectionString)
		if err != nil {
			log.Errorln(err)
			log.Errorf("Waiting %d seconds before retry connecting", 5)
			time.Sleep(time.Duration(5) * time.Second)
			continue
		}
		ch, err = conn.Channel()
		if err != nil {
			log.Errorln(err)
			log.Errorf("Waiting %d seconds before retry connecting", 5)
			time.Sleep(time.Duration(5) * time.Second)
			continue
		}

		if err == nil {
			// log.Println("Connection to rabbit has been established")
			return ch, conn, err
		}
	}
}

// Publish expose method
func Publish(ch *amqp.Channel, queueName string, message []byte) int {
	// log.Infof("Push to RabbitMQ queue: %s", queueName)

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// body := scheduleString
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
		})
	failOnError(err, "Failed to publish a message")
	if err == nil {
		return 1
	}
	return 0
}
