package airutil

import (
	"math/rand"
	"net/http"
	"reflect"
	"sync"
	"testing"

	"github.com/streadway/amqp"
	schema "gitlab.com/neveragny/airutil/lib"
	mgo "gopkg.in/mgo.v2"
)

func TestCurrencyConvert(t *testing.T) {
	var v float64
	amount := rand.Float64()
	v = ToEUR(amount, "EUR")
	if v != amount {
		t.Error("Expected amount, got ", v)
	}
}

func Test_IntToOffset(t *testing.T) {
	var inputVsExpected = map[int]string{
		1:   "+0100",
		-1:  "-0100",
		10:  "+1000",
		-12: "-1200",
		-0:  "+0000",
	}

	var input int
	var result, expectedResult string
	var err error

	for key, val := range inputVsExpected {
		input = key
		expectedResult = val

		result, err = IntToOffset(input)
		if err != nil {
			t.Errorf("Error returned %v", err)
		}

		if result != expectedResult {
			t.Errorf("got %s, expected %s", result, expectedResult)
		}

	}
	// var n string
	// inpN := 1
	// n, err := IntToOffset(inpN)

	// if n != "+0100" {
	// 	t.Errorf("got %s, expected +0100", n)
	// }

	// inpN = 1
	// n, err = IntToOffset(inpN)
	// if err != nil {
	// 	t.Errorf("Error returned %v", err)
	// }

	// if n != "+0100" {
	// 	t.Errorf("got %s, expected +0100", n)
	// }
}

func Test_publish(t *testing.T) {
	type args struct {
		ch        *amqp.Channel
		queueName string
		message   []byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := publish(tt.args.ch, tt.args.queueName, tt.args.message); got != tt.want {
				t.Errorf("publish() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_runPriceForRoute(t *testing.T) {
	type args struct {
		airline        string
		apiQuery       APIQuery
		apiParams      APIParams
		parse          ParseResponse
		crawlerOptions *SpecCrawlerConf
		cookie         *http.Cookie
		redisConfig    *schema.RabbitMQConf
		waitGroup      *sync.WaitGroup
		jobs           <-chan []string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runPriceForRoute(tt.args.airline, tt.args.apiQuery, tt.args.apiParams, tt.args.parse, tt.args.crawlerOptions, tt.args.cookie, tt.args.redisConfig, tt.args.waitGroup, tt.args.jobs)
		})
	}
}

func Test_readConfig(t *testing.T) {
	type args struct {
		configPath string
	}
	tests := []struct {
		name    string
		args    args
		want    *mgo.DialInfo
		want1   *schema.RabbitMQConf
		want2   *schema.AirConf
		want3   *schema.ProxyConf
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2, got3, err := readConfig(tt.args.configPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("readConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readConfig() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("readConfig() got1 = %v, want %v", got1, tt.want1)
			}
			if !reflect.DeepEqual(got2, tt.want2) {
				t.Errorf("readConfig() got2 = %v, want %v", got2, tt.want2)
			}
			if !reflect.DeepEqual(got3, tt.want3) {
				t.Errorf("readConfig() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}

func Test_getCrawlerOptions(t *testing.T) {
	type args struct {
		airline      string
		mongoSession *mgo.Session
	}
	tests := []struct {
		name string
		args args
		want *SpecCrawlerConf
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getCrawlerOptions(tt.args.airline, tt.args.mongoSession); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getCrawlerOptions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getRoutes(t *testing.T) {
	type args struct {
		airline      string
		mongoSession *mgo.Session
	}
	tests := []struct {
		name    string
		args    args
		want    []Destination
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getRoutes(tt.args.airline, tt.args.mongoSession)
			if (err != nil) != tt.wantErr {
				t.Errorf("getRoutes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getRoutes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPushRoutesTasks(t *testing.T) {
	type args struct {
		airline    string
		configPath string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			PushRoutesTasks(tt.args.airline, tt.args.configPath)
		})
	}
}

func TestPricesFlow(t *testing.T) {
	type args struct {
		airline           string
		queryLambda       APIQuery
		queryParamsLambda APIParams
		parseLambda       ParseResponse
		configPath        string
		mode              string
		cookieChan        chan *http.Cookie
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			PricesFlow(tt.args.airline, tt.args.queryLambda, tt.args.queryParamsLambda, tt.args.parseLambda, tt.args.configPath, tt.args.mode, tt.args.cookieChan)
		})
	}
}
