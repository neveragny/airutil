package schema

import (
	"fmt"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Destination schema
type Destination struct {
	Origin      string `json:"origin_code" bson:"origin_code"`
	Destination string `json:"destination_code" bson:"destination_code"`
	Airline     string `json:"airline" bson:"airline"`
}

// Schedule collection schema
type Schedule struct {
	ID              bson.ObjectId       `json:"id" bson:"_id,omitempty"`
	Origin          string              `json:"origin" bson:"origin"`
	Destination     string              `json:"destination" bson:"destination"`
	Price           float64             `json:"price" bson:"price"`
	Airline         string              `json:"airline" bson:"airline"`
	Date            time.Time           `json:"date" bson:"date"`
	ArrivalDateTime time.Time           `json:"arrival_date_time" bson:"arrival_date_time"`
	UpdatedAt       time.Time           `json:"updated_at" bson:"updated_at"`
	Accomodation    AccomodationDetails `json:"accomodation" bson:"accomodation"`
}

func (s Schedule) String() string {
	return fmt.Sprintf("{Origin: %s, Destination: %s, Price: %f, Date: %s}", s.Origin, s.Destination, s.Price, s.Date)
}

// Airport collection schema
type Airport struct {
	Name        string    `json:"name"`
	City        string    `json:"city"`
	Code        string    `json:"code"`
	Latitude    float64   `json:"latitude"`
	Longtitude  float64   `json:"longtitude"`
	Coordinates []float64 `json:"coordinates"`
}

// ScheduleLookupOptions schema
type ScheduleLookupOptions struct {
	Origin      string
	Destination string
	Date        time.Time
}

// RoundFlight collection schema
type RoundFlight struct {
	ID               bson.ObjectId `json:"id" bson:"_id,omitempty"`
	ScheduleID       bson.ObjectId `json:"schedule_id" bson:"schedule_id"`
	Origin           string        `json:"origin" bson:"origin"`
	Destination      string        `json:"destination" bson:"destination"`
	Price            float64       `json:"price" bson:"price"`
	Airline          string        `json:"airline" bson:"airline"`
	Date             time.Time     `json:"date" bson:"date"`
	UpdatedAt        time.Time     `json:"updated_at" bson:"updated_at"`
	RoundFlightPrice float64       `json:"round_flight_price" bson:"round_flight_price"`
	Total            float64       `json:"total" bson:"total"`
	AffiliateURL     string        `json:"affiliate_url" bson:"affiliate_url"`
	Return           Schedule      `json:"return" bson:"return"`
}

// LookupRoundFlight collections schema
type LookupRoundFlight struct {
	ScheduleID       bson.ObjectId `json:"schedule_id" bson:"schedule_id"`
	ReturnScheduleID bson.ObjectId `json:"return._id" bson:"return._id"`
}

// Task struct to send in channel
type Task struct {
	ID      int
	Data    []string
	Session *mgo.Session
}

// AccomodationRequest with booking data
type AccomodationRequest struct {
	CheckinMonth     int
	CheckinMonthDay  int
	CheckinYear      int
	CheckoutMonth    int
	CheckoutMonthDay int
	CheckoutYear     int
	DestType         string
	NoRooms          int
	GroupAdults      int
	Order            string
	SS               string
}

// AccomodationDetails ..
type AccomodationDetails struct {
	Name         string    `json:"name" bson:"name"`
	Score        float64   `json:"score" bson:"score"`
	Price        int       `json:"price" bson:"price"`
	CheckInDate  string    `json:"checkindate" bson:"checkindate"`
	CheckoutDate string    `json:"checkoutdate" bson:"checkoutdate"`
	StartAmount  string    `json:"startamount" bson:"startamount"`
	DeepLink     string    `json:"deeplink" bson:"deeplink"`
	UpdatedAt    time.Time `json:"updated_at" bson:"updated_at"`
}

// AirConf for crawler specific params
type AirConf struct {
	Out        string `json:"out"`
	NumWorkers int    `json:"num_workers"`
}

// Config file schema
type Config struct {
	DB       DBConnection `json:"db"`
	RabbitMQ RabbitMQConf `json:"rabbitmq"`
	Proxy    ProxyConf    `json:"proxy"`
	Air      AirConf      `json:"air"`
}

// DBConnection block on config schema
type DBConnection struct {
	Hosts    []string      `json:"hosts"`
	Name     string        `json:"name"`
	Username string        `json:"username"`
	Password string        `json:"password"`
	Timeout  time.Duration `json:"timeout"`
}

// RabbitMQConf connection details
type RabbitMQConf struct {
	Hosts    []string `json:"hosts"`
	Username string   `json:"username"`
	Password string   `json:"password"`
	Retry    int      `json:"retry"`
}

// ProxyConf proxy connection details
type ProxyConf struct {
	URL string `json:"url"`
}
