package airutil

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/streadway/amqp"
	schema "gitlab.com/neveragny/airutil/lib"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var airline string

// APIQuery describe passed lambda for custom airline API request processing
type APIQuery func(string, string, *http.Cookie) (string, error)

// APIParams describe passed lambda for custom airline API data request processing
type APIParams func(time.Time, string, string, string) string

// ParseResponse describe custom parse method passed by 'airline' parser
type ParseResponse func(string, string, string) ([]*Schedule, *time.Time, error)

// DateRangeLambda function lambda
type DateRangeLambda func(dep, arr string) ([]string, error)

// Crawler ...
type Crawler struct {
	airlineName string
}

// Airport mapping strunct to airports colection in mongo
type Airport struct {
	AirportID  int     `json:"airport_id" bson:"airport_id"`
	Name       string  `json:"name" bson:"name"`
	City       string  `json:"city" bson:"city"`
	Country    string  `json:"country" bson:"country"`
	Code       string  `json:"code" bson:"code"`
	Icao       string  `json:"icao" bson:"icao"`
	Latitude   float64 `json:"latitude" bson:"latitude"`
	Longtitude float64 `json:"longtitude" bson:"longtitude"`
	Altitude   int     `json:"altitude" bson:"altitude"`
	Timezone   int     `json:"timezone" bson:"timezone"`
	Dst        string  `json:"dst" bson:"dst"`
	TzFormat   string  `json:"tz_format" bson:"tz_format"`
}

// Proxy collection schema
type Proxy struct {
	URL       string    `json:"url" bson:"url"`
	Status    string    `json:"status" bson:"status"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

// Destination collection schema
type Destination struct {
	ID              bson.ObjectId `bson:"_id,omitempty"`
	OriginCode      string        `bson:"origin_code"`
	DestinationCode string        `bson:"destination_code"`
	Airline         string        `bson:"airline"`
}

// SpecCrawlerConf ..
type SpecCrawlerConf struct {
	Airline string                 `bson:"airline"`
	Config  SpecCrawlerAirlineConf `bson:"config"`
}

// SpecCrawlerAirlineConf crawler specific properties
type SpecCrawlerAirlineConf struct {
	AvailabilityURL string `bson:"availability_url"`
	APIKey          string `bson:"apikey"`
}

// Schedule collection schema
type Schedule struct {
	ID                   string    `json:"id" bson:"_id,omitempty"`
	Origin               string    `json:"origin" bson:"origin"`
	Destination          string    `json:"destination" bson:"destination"`
	FlightNumber         string    `json:"flight_number" bson:"flight_number"`
	Duration             string    `json:"duration" bson:"duration"`
	Price                float64   `json:"price" bson:"price"`
	Airline              string    `json:"airline" bson:"airline"`
	Date                 time.Time `json:"date" bson:"date"`
	DepartureDateTime    time.Time `json:"departure_date_time" bson:"departure_date_time"`
	ArrivalDateTime      time.Time `json:"arrival_date_time" bson:"arrival_date_time"`
	DepartureDateTimeUTC time.Time `json:"departure_date_time_utc" bson:"departure_date_time_utc"`
	ArrivalDateTimeUTC   time.Time `json:"arrival_date_time_utc" bson:"arrival_date_time_utc"`
	UpdatedAt            time.Time `json:"updated_at" bson:"updated_at"`
	CheapestRoundPrice   float64   `json:"cheapest_round_price" bson:"cheapest_round_price"`
	AffiliateURL         string    `json:"affiliate_url" bson:"affiliate_url"`
}

// LookupSchedule schema
type LookupSchedule struct {
	Origin      string    `json:"origin" bson:"origin"`
	Destination string    `json:"destination" bson:"destination"`
	Airline     string    `json:"airline" bson:"airline"`
	Date        time.Time `json:"date" bson:"date"`
}

// PriceForRouteParams struct
type PriceForRouteParams struct {
	Airline         string
	APIQuery        APIQuery
	APIParams       APIParams
	DateRangeLambda DateRangeLambda
	ParseLambda     ParseResponse
	CrawlerOptions  *SpecCrawlerConf
	Cookie          *http.Cookie
	RabbitMQConfig  *schema.RabbitMQConf
	Job             []string
}

// ToEUR convert amount of currency to EUR
func ToEUR(amount float64, currencyCode string) float64 {
	log.Debugf("amount %f currecyCode %s", amount, currencyCode)
	switch currencyCode {
	case "EUR":
		return amount
	case "USD":
		return amount * 0.85
	case "UAH":
		return amount * 0.032
	case "BGN":
		return amount * 0.51 // Bolgaria Lev
	case "BAM":
		return amount * 0.53
	case "CZK":
		return amount * 0.039
	case "GEL":
		return amount * 0.35
	case "HUF":
		return amount * 0.0031
	case "ILS":
		return amount * 0.24
	case "MKD":
		return amount * 0.016
	case "NOK":
		return amount * 0.11
	case "PLN":
		return amount * 0.23
	case "RON":
		return amount * 0.21
	case "DKK":
		return amount * 0.13
	default:
		return 0.0
	}
	// return 0.0
}

// IntToOffset returns offset string
func IntToOffset(num int) (string, error) {
	var resultOffset string

	plusMinus := "+"
	if num < 0 {
		plusMinus = "-"
		num = num * -1
	}

	padNum := fmt.Sprintf("%02d", num)
	resultOffset = fmt.Sprintf("%s%s00", plusMinus, padNum)

	return resultOffset, nil
}

// InsertSchedule - public method for insert record in schedules collection
func InsertSchedule(session *mgo.Session, schedule *Schedule) error {
	return insertSchedule(session, schedule)
}

func insertSchedule(session *mgo.Session, schedule *Schedule) error {
	c := session.DB("air").C("schedules")
	lookupEntry := &LookupSchedule{
		Origin:      schedule.Origin,
		Destination: schedule.Destination,
		Airline:     schedule.Airline,
		Date:        schedule.Date,
	}
	schedule.UpdatedAt = time.Now()
	log.Info("Lookup entry: ", lookupEntry)
	log.Info("Schedule INSERT entry: ", schedule)
	changeInfo, err := c.Upsert(lookupEntry, schedule)
	log.Info("Change info: ", changeInfo)
	return err
}

// FailOnError exported
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

// DialRabbitmq return shared connection link
func DialRabbitmq(conf *schema.RabbitMQConf) (*amqp.Channel, *amqp.Connection, error) {
	return dialRabbitmq(conf)
}

func dialRabbitmq(conf *schema.RabbitMQConf) (*amqp.Channel, *amqp.Connection, error) {
	log.Println(conf.Password)
	log.Println(conf.Username)
	log.Println(conf.Hosts)
	rabbitConnectionString := fmt.Sprintf("amqp://%s:%s@%s", conf.Username, conf.Password, conf.Hosts[0])
	// log.Println(rabbitConnectionString)
	var conn *amqp.Connection
	var ch *amqp.Channel
	var err error
	for {
		conn, err = amqp.Dial(rabbitConnectionString)
		if err != nil {
			log.Errorln(err)
			log.Errorf("Waiting %d seconds before retry connecting", 5)
			time.Sleep(time.Duration(5) * time.Second)
			continue
		}
		ch, err = conn.Channel()
		if err != nil {
			log.Errorln(err)
			log.Errorf("Waiting %d seconds before retry connecting", 5)
			time.Sleep(time.Duration(5) * time.Second)
			continue
		}

		if err == nil {
			// log.Println("Connection to rabbit has been established")
			return ch, conn, err
		}
	}
}

//PublishToAMQP exported method
func PublishToAMQP(ch *amqp.Channel, queueName string, message []byte) int {
	return publish(ch, queueName, message)
}
func publish(ch *amqp.Channel, queueName string, message []byte) int {
	// log.Infof("Push to RabbitMQ queue: %s", queueName)

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	FailOnError(err, "Failed to declare a queue")

	// body := scheduleString
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
		})
	FailOnError(err, "Failed to publish a message")
	if err == nil {
		return 1
	}
	return 0
}

func runPriceForRoute(params *PriceForRouteParams) {
	log.Println("entering runPricesForRoute")

	ch, conn, err := dialRabbitmq(params.RabbitMQConfig)
	if err != nil {
		panic(err)
	}
	defer ch.Close()
	defer conn.Close()

	availabilityURL := params.CrawlerOptions.Config.AvailabilityURL
	apiKey := params.CrawlerOptions.Config.APIKey
	nextDay := time.Now()

	// for j := range jobs {

	time.Sleep(time.Second / 10)
	start := time.Now()

	dateRange, err := params.DateRangeLambda(params.Job[0], params.Job[1])
	if dateRange != nil && len(dateRange) > 0 {
		log.Println("dateRange is defined, start working on it")
		for _, date := range dateRange {
			fmt.Println(date)
			// Mon Jan 2 15:04:05 MST 2006
			layout := "2006-01-02"
			d, err := time.Parse(layout, date)
			if err != nil {
				log.Fatal(err)
			}
			queryString := params.APIParams(d, params.Job[0], params.Job[1], apiKey)
			retryCount, retryMax := 0, 3
			var jsonData string

			for retryCount = 0; retryCount < retryMax; retryCount++ {
				log.Debugf("apiQuery attempt # %d", retryCount)
				jsonData, err = params.APIQuery(availabilityURL, queryString, params.Cookie)
				if err != nil {
					log.Errorf("Error while making apiQuery: %+v\n", err)
					// retryCount++
					if retryCount > retryMax {
						break
					}
					continue
				}
				break
			}

			schedules, _, err := params.ParseLambda(jsonData, params.Job[0], params.Job[1])
			if err != nil || schedules == nil {
				log.Errorln(err)
				continue
			}
			for _, schedule := range schedules {
				if schedule == nil {
					continue
				}
				scheduleString, err := json.Marshal(schedule)
				if err != nil {
					log.Errorln(err)
				}
				publish(ch, "crawled", scheduleString)
			}
		}
		return
	}
	// log.Info("Start processing on ROUTE: ", j)
	if airline == "transavia" {
		for d := start; d.Before(start.AddDate(0, 13, 0)); d = d.AddDate(0, 1, 0) {
			log.Debugln("Date loop CONTINUE ON :", d)
			log.Debugln("Date loop CONTINUE ON :", d)

			log.Debugln("d: ", d)
			queryString := params.APIParams(d, params.Job[0], params.Job[1], apiKey)
			log.Debugln(fmt.Sprintf("%s?%s", availabilityURL, queryString))
			log.Debugln(">>>> DEBUG <<<< : ", params.Cookie)
			jsonData, err := params.APIQuery(availabilityURL, queryString, params.Cookie)
			if err != nil {
				log.Errorf("Error while making apiQuery: %+v\n", err)
				continue
			}
			schedules, _, err := params.ParseLambda(jsonData, params.Job[0], params.Job[1])

			if err != nil || schedules == nil {
				log.Errorln(err)
				continue
			}
			for _, schedule := range schedules {
				if schedule == nil {
					continue
				}
				scheduleString, err := json.Marshal(schedule)
				if err != nil {
					log.Errorln(err)
				}
				publish(ch, "crawled", scheduleString)
			}
		}
	} else {
		log.Println("airline is not transavia... proceeding")
		for d := start; d.Before(start.AddDate(0, 0, 365)); d = d.AddDate(0, 0, 1) {
			if nextDay.After(d) {
				log.Debugf("\nnextDay: %s\nd: %s\nnextDay.After(d): %v\n> SKIPPING <", nextDay, d, nextDay.After(d))
				continue
			}
			log.Debugf("Date loop CONTINUE ON %v", d)
			// layout := "Jan 2, 2006 at 3:04pm (MST)"
			//layout := "2006-01-02"

			log.Debugln("d: ", d)
			log.Debugln("j: ", params.Job)
			queryString := params.APIParams(d, params.Job[0], params.Job[1], apiKey)
			log.Debugln(fmt.Sprintf("%s?%s", availabilityURL, queryString))

			// log.Debugln(">>>> DEBUG <<<< : ", cookie)
			jsonData, err := params.APIQuery(availabilityURL, queryString, params.Cookie)
			if err != nil {
				log.Errorf("apiQuery request failed: %v\n", err)
				continue
			}

			// if jsonData == "{}" {
			// 	publish(ch, fmt.Sprintf("%s_failed_urls", airline), []byte(fmt.Sprintf("%s?%s", availabilityURL, queryString)))
			// }

			schedules, lastDate, err := params.ParseLambda(jsonData, params.Job[0], params.Job[1])

			if err != nil || schedules == nil {
				log.Errorln(err)
				continue
			}
			nextDay = *lastDate
			for _, schedule := range schedules {
				if schedule == nil {
					continue
				}
				scheduleString, err := json.Marshal(schedule)
				if err != nil {
					log.Errorln(err)
				}
				publish(ch, "crawled", scheduleString)
			}
		} // END OF LOOP over YEAR dates
	}

	nextDay = time.Now()
	log.Info("Done processing on ROUTE: ", params.Job)
	// }
}

// ReadConfig return number of Config structs: mongoConfig, rabbitMQConfig, proxyConfig, etc
func ReadConfig(configPath string) (*mgo.DialInfo, *schema.RabbitMQConf, *schema.ProxyConf, *schema.AirConf, error) {
	return readConfig(configPath)
}

func readConfig(configPath string) (*mgo.DialInfo, *schema.RabbitMQConf, *schema.ProxyConf, *schema.AirConf, error) {
	file, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatalf("File error: %v\n", err)
	}
	log.Debugf("config file path %s", string(file))
	var jsontype schema.Config
	json.Unmarshal(file, &jsontype)
	log.Debugf("Results: %v\n", jsontype)

	log.Debugln("jsontype.DB.Hosts: ", jsontype.DB.Hosts)
	log.Debugln("jsontype.DB.Name: ", jsontype.DB.Name)
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    jsontype.DB.Hosts,
		Database: jsontype.DB.Name,
		Timeout:  jsontype.DB.Timeout,
	}

	if jsontype.DB.Username != "" && jsontype.DB.Password != "" {
		mongoDBDialInfo.Username = jsontype.DB.Username
		mongoDBDialInfo.Password = jsontype.DB.Password
	}

	rabbitConf := jsontype.RabbitMQ
	proxyConf := jsontype.Proxy
	airConf := jsontype.Air

	return mongoDBDialInfo, &rabbitConf, &proxyConf, &airConf, err
}

// GetCrawlerOptions exported function
func GetCrawlerOptions(airline string, mongoSession *mgo.Session) *SpecCrawlerConf {
	return getCrawlerOptions(airline, mongoSession)
}

// getCrawlerOptions retrieve crawler config options from db
func getCrawlerOptions(airline string, mongoSession *mgo.Session) *SpecCrawlerConf {
	c := mongoSession.DB("")
	var config *SpecCrawlerConf
	err := c.C("configs").Find(bson.M{"airline": airline}).One(&config)
	if err != nil {
		log.Fatal("Cant getCrawlerOptions \nErr: ", err)
	}
	log.Debugln(config)
	return config
}

func getRoutes(airline string, mongoSession *mgo.Session) ([]Destination, error) {
	var destinations []Destination
	c := mongoSession.DB("")
	err := c.C("destinations").Find(bson.M{"airline": airline}).All(&destinations)
	if err != nil {
		log.Fatal(err)
	}
	log.Debugln(destinations)
	return destinations, err
}

// GetAirports return data from airports collection in mongo
func GetAirports(configPath string) (map[string]Airport, error) {
	mongoConfig, _, _, _, _ := readConfig(configPath)
	log.Debug(mongoConfig)

	// Dial Mongo
	mongoSession, err := mgo.DialWithInfo(mongoConfig)
	if err != nil {
		log.Fatalf("CreateSession: %s\n", err)
	}
	mongoSession.SetMode(mgo.Monotonic, true)
	defer mongoSession.Close()
	var airports []Airport
	c := mongoSession.DB("")
	err = c.C("airports").Find(bson.M{}).All(&airports)
	if err != nil {
		log.Fatal(err)
	}
	if len(airports) == 0 {
		return nil, err
	}

	airportDict := make(map[string]Airport)
	for _, airport := range airports {
		airportDict[airport.Code] = airport
	}

	return airportDict, err
}

// PushRoutesTasks pushing url tasks to queue
func PushRoutesTasks(airline string, configPath string) {
	fmt.Println(configPath)
	mongoConfig, rabbitMQConfig, _, _, _ := readConfig(configPath)
	log.Println(mongoConfig)
	for {
		// Dial Mongo
		mongoSession, err := mgo.DialWithInfo(mongoConfig)
		if err != nil {
			log.Fatalf("CreateSession: %s\n", err)
		}
		mongoSession.SetMode(mgo.Monotonic, true)
		defer mongoSession.Close()
		results, err := getRoutes(airline, mongoSession)
		log.Printf("received %d number of routes, preparing to push in rabbitMQ", len(results))

		// Dial RabbitMq
		ch, conn, err := dialRabbitmq(rabbitMQConfig)
		if err != nil {
			FailOnError(err, "Failed to get conn, channel from DialRabbitmq()")
		}
		defer func() {
			log.Println("close goddamit")
			ch.Close()
			conn.Close()
		}()

		airlineQueue := fmt.Sprintf("%s_url_jobs", airline)
		_, err = ch.QueueDeclare(
			airlineQueue, // name
			false,        // durable
			false,        // delete when unused
			false,        // exclusive
			false,        // no-wait
			nil,          // arguments
		)

		qi, err := ch.QueueInspect(airlineQueue)
		if err != nil {
			FailOnError(err, fmt.Sprintf("failed to inspect queue %s\n", airlineQueue))
		}
		log.Printf("%s queue message count %d", airlineQueue, qi.Messages)
		if qi.Messages > 5 { // 5 just because I like it
			time.Sleep(time.Second * 30)
			continue
		}

		// Start pushing to the limit(c)
		counter := 0
		for _, item := range results {
			log.Debugf("Origin: %s, Destination: %s, airline: %s\n", item.OriginCode, item.DestinationCode, item.Airline)
			log.Debugf("Going to push %v\n", item)
			route := fmt.Sprintf("%s-%s", item.OriginCode, item.DestinationCode)
			counter += publish(ch, airlineQueue, []byte(route))
		}
		log.Infof("Pushed %d routes", counter)
		ch.Close()
		conn.Close()
		time.Sleep(time.Second * 30)
	}
}

// PricesFlow initial call to lookup routes from DB by airline name and send
// them to channel to be picked up by runPriceForRoute goroutine
func PricesFlow(
	airline string,
	queryRequestLambda APIQuery,
	queryParamsLambda APIParams,
	parseLambda ParseResponse,
	dateRangeLambda DateRangeLambda,
	configPath string,
	mode string,
	cookieChan chan *http.Cookie) {

	mongoConfig, rabbitConfig, proxyConf, _, err := readConfig(configPath)
	if err != nil {
		log.Errorln(err)
	}
	log.Debugf("mongoConfig: %+v", mongoConfig)
	log.Infof("proxyConfig: %+v", proxyConf)

	// Dial Mongo
	mongoSession, err := mgo.DialWithInfo(mongoConfig)
	if err != nil {
		log.Fatalf("CreateSession: %s\n", err)
	}
	mongoSession.SetMode(mgo.Monotonic, true)

	// Read crawler specific properties from mongo collection "configs"
	crawlerOptions := getCrawlerOptions(airline, mongoSession)
	mongoSession.Close()
	log.Info("crawlerOptions.AvailabilityURL: ", crawlerOptions.Config.AvailabilityURL)
	// os.Exit(0)

	log.Info("Starting things up..")
	log.Info("Create jobs channel")
	// jobs := make(chan []string, airConfig.NumRo)

	ch, conn, err := dialRabbitmq(rabbitConfig)
	if err != nil {
		FailOnError(err, "Failed to get conn, channel from DialRabbitmq()")
	}
	defer func() {
		ch.Close()
		conn.Close()
	}()

	// tells RabbitMQ not to give more than one message to a worker at a time
	// https://www.rabbitmq.com/tutorials/tutorial-two-go.html
	err = ch.Qos(1, 0, false)
	if err != nil {
		panic(err)
	}

	var cookie *http.Cookie

	if cookieChan != nil {
		log.Info(cookieChan)
		cookie = <-cookieChan
	}

	// https://godoc.org/github.com/streadway/amqp#Channel.QueueDeclare
	// Declare queue even it doesn't exist, as promised this idempotent action
	q, err := ch.QueueDeclare(
		fmt.Sprintf("%s_url_jobs", airline), // name
		false,                               // durable
		false,                               // delete when unused
		false,                               // exclusive
		false,                               // no-wait
		nil,                                 // arguments
	)

	deliveries, err := ch.Consume(
		q.Name, //queue
		"",     //consumerTag
		false,  //auto-ack
		false,  //exclusive
		false,  //no-local
		false,  //no-wait
		nil)    //args

	if err != nil {
		log.Errorln(err)
	}

	for d := range deliveries {
		// log.Printf("Message recived in fanout %d", fanoutNum)
		log.Printf("Received a message: %s", d.Body)
		d.Ack(true)
		// airline string, apiQuery APIQuery, apiParams APIParams, parse ParseResponse, crawlerOptions *SpecCrawlerConf, cookie *http.Cookie, rabbitMQConfig *schema.RabbitMQConf, job []string
		priceForRouteParams := &PriceForRouteParams{
			Airline:         airline,
			APIQuery:        queryRequestLambda,
			APIParams:       queryParamsLambda,
			ParseLambda:     parseLambda,
			DateRangeLambda: dateRangeLambda,
			CrawlerOptions:  crawlerOptions,
			Cookie:          cookie,
			RabbitMQConfig:  rabbitConfig,
			Job:             strings.Split(string(d.Body), "-"),
		}
		runPriceForRoute(priceForRouteParams)
	}
}
